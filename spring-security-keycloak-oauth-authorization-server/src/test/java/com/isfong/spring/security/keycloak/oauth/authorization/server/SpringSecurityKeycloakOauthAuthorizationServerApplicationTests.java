package com.isfong.spring.security.keycloak.oauth.authorization.server;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringSecurityKeycloakOauthAuthorizationServerApplicationTests {

	@Test
	void contextLoads() {
	}

}
