package com.isfong.spring.security.keycloak.oauth.authorization.server;

import org.keycloak.common.ClientConnection;
import org.keycloak.services.filters.AbstractRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class EmbeddedKeycloakRequestFilter extends AbstractRequestFilter implements Filter {

    @Override
    public void doFilter( ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws IOException {
        servletRequest.setCharacterEncoding( "UTF-8" );
        ClientConnection clientConnection = createConnection( ( HttpServletRequest ) servletRequest );

        filter( clientConnection, ( session ) -> {
            try {
                filterChain.doFilter( servletRequest, servletResponse );
            } catch ( Exception e ) {
                throw new RuntimeException( e );
            }
        } );
    }

    private ClientConnection createConnection( HttpServletRequest request ) {
        return new ClientConnection( ) {
            @Override
            public String getRemoteAddr( ) {
                return request.getRemoteAddr( );
            }

            @Override
            public String getRemoteHost( ) {
                return request.getRemoteHost( );
            }

            @Override
            public int getRemotePort( ) {
                return request.getRemotePort( );
            }

            @Override
            public String getLocalAddr( ) {
                return request.getLocalAddr( );
            }

            @Override
            public int getLocalPort( ) {
                return request.getLocalPort( );
            }
        };
    }
}
