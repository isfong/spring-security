package com.isfong.spring.security.keycloak.oauth.authorization.server;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties( "keycloak.oauth" )
public class KeycloakOAuthProperties {
    String contextPath = "/oauth";
    String realmImportFile = "realm.json";
    Manager manager = new Manager( );

    @Setter
    @Getter
    public static class Manager {
        String username = "admin";
        String password = "admin";
    }
}
