package com.isfong.spring.security.keycloak.oauth.authorization.server;

import org.jboss.resteasy.plugins.server.servlet.HttpServlet30Dispatcher;
import org.jboss.resteasy.plugins.server.servlet.ResteasyContextParameters;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.naming.*;
import javax.naming.spi.NamingManager;
import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties( KeycloakOAuthProperties.class )
public class KeycloakConfiguration {

    @Bean
    ServletRegistrationBean< HttpServlet30Dispatcher > keycloakJaxRsApplication( KeycloakOAuthProperties keycloakOAuthProperties, DataSource dataSource ) throws NamingException {
        this.mockJndiEnvironment( dataSource );
        EmbeddedKeycloakApplication.keycloakOAuthProperties = keycloakOAuthProperties;
        ServletRegistrationBean< HttpServlet30Dispatcher > servlet = new ServletRegistrationBean<>( new HttpServlet30Dispatcher( ) );
        servlet.addInitParameter( "javax.ws.rs.Application", EmbeddedKeycloakApplication.class.getName( ) );
        servlet.addInitParameter( ResteasyContextParameters.RESTEASY_SERVLET_MAPPING_PREFIX, keycloakOAuthProperties.getContextPath( ) );
        servlet.addInitParameter( ResteasyContextParameters.RESTEASY_USE_CONTAINER_FORM_PARAMS, "true" );
        servlet.addUrlMappings( keycloakOAuthProperties.getContextPath( ) + "/*" );
        servlet.setLoadOnStartup( 1 );
        servlet.setAsyncSupported( true );
        return servlet;
    }

    @Bean
    FilterRegistrationBean< EmbeddedKeycloakRequestFilter > keycloakSessionManagement( KeycloakOAuthProperties keycloakOAuthProperties ) {
        FilterRegistrationBean< EmbeddedKeycloakRequestFilter > filter = new FilterRegistrationBean<>( );
        filter.setName( "Keycloak Session Management" );
        filter.setFilter( new EmbeddedKeycloakRequestFilter( ) );

        return filter;
    }

    private void mockJndiEnvironment( DataSource dataSource ) throws NamingException {
        NamingManager.setInitialContextFactoryBuilder( env -> environment -> new InitialContext( ) {
            @Override
            public Object lookup( Name name ) {
                return lookup( name.toString( ) );
            }

            @Override
            public Object lookup( String name ) {

                if ( "spring/datasource".equals( name ) ) {
                    return dataSource;
                }

                return null;
            }

            @Override
            public NameParser getNameParser( String name ) {
                return CompositeName::new;
            }

            @Override
            public void close( ) {
                // NOOP
            }
        } );
    }
}
