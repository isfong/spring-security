package com.isfong.spring.security.keycloak.oauth.authorization.server;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.services.managers.ApplianceBootstrap;
import org.keycloak.services.managers.RealmManager;
import org.keycloak.services.resources.KeycloakApplication;
import org.keycloak.services.util.JsonConfigProviderFactory;
import com.isfong.spring.security.keycloak.oauth.authorization.server.KeycloakOAuthProperties.Manager;
import org.keycloak.util.JsonSerialization;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.NoSuchElementException;

@Slf4j
public class EmbeddedKeycloakApplication extends KeycloakApplication {
    static KeycloakOAuthProperties keycloakOAuthProperties;

    public EmbeddedKeycloakApplication( ) {
        super( );
        this.createMasterRealm( );
        this.createRealm( );
    }

    protected void loadConfig( ) {
        JsonConfigProviderFactory factory = new RegularJsonConfigProviderFactory( );
        Config.init( factory.create( )
                .orElseThrow( ( ) -> new NoSuchElementException( "No value present" ) ) );
    }

    private void createMasterRealm( ) {
        KeycloakSessionFactory sessionFactory = getSessionFactory( );
        KeycloakSession session = sessionFactory.create( );
        ApplianceBootstrap applianceBootstrap = new ApplianceBootstrap( session );
        Manager manager = keycloakOAuthProperties.getManager( );
        try {
            session.getTransactionManager( ).begin( );
            applianceBootstrap.createMasterRealmUser( manager.getUsername( ), manager.getPassword( ) );
            session.getTransactionManager( ).commit( );
        } catch ( Exception ex ) {
            log.warn( "Couldn't create keycloak master admin user: {}", ex.getMessage( ) );
            session.getTransactionManager( )
                    .rollback( );
        }
        session.close( );
    }

    private void createRealm( ) {
        KeycloakSession session = getSessionFactory( ).create( );
        try {
            session.getTransactionManager( ).begin( );
            RealmManager manager = new RealmManager( session );
            Resource lessonRealmImportFile = new ClassPathResource( keycloakOAuthProperties.getRealmImportFile( ) );
            manager.importRealm( JsonSerialization.readValue( lessonRealmImportFile.getInputStream( ), RealmRepresentation.class ) );
            session.getTransactionManager( ).commit( );
        } catch ( Exception ex ) {
            log.warn( "Failed to import Realm json file: {}", ex.getMessage( ) );
            session.getTransactionManager( ).rollback( );
        }
        session.close( );
    }
}
