package com.isfong.spring.security.keycloak.oauth.authorization.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;

@SpringBootApplication( exclude = LiquibaseAutoConfiguration.class )
public class SpringSecurityKeycloakOauthAuthorizationServerApplication {
//    private static final Logger LOG = LoggerFactory.getLogger( SpringSecurityKeycloakOauthAuthorizationServerApplication.class );

    public static void main( String[] args ) {
        SpringApplication.run( SpringSecurityKeycloakOauthAuthorizationServerApplication.class, args );
    }

//    @Bean
//    ApplicationListener< ApplicationReadyEvent > onApplicationReadyEventListener( ServerProperties serverProperties,
//                                                                                  KeycloakOAuthProperties keycloakOAuthProperties ) {
//        return ( evt ) -> {
//
//            Integer port = serverProperties.getPort( );
//            String keycloakContextPath = keycloakOAuthProperties.getContextPath( );
//            LOG.info( "Embedded Keycloak started: http://localhost:{}{} to use keycloak", port, keycloakContextPath );
//        };
//    }

}
