package com.isfong.spring.security.keycloak.oauth.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class SpringSecurityKeycloakOauthClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityKeycloakOauthClientApplication.class, args);
	}

}
