package com.isfong.spring.security.keycloak.oauth.client.resource;

import com.isfong.spring.security.keycloak.oauth.client.domain.Customer;
import com.isfong.spring.security.keycloak.oauth.client.infrastructure.persistence.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequiredArgsConstructor
@RequestMapping( "/customers" )
public class CustomerResource {

    private final CustomerRepository customerRepository;

    @GetMapping
    public ResponseEntity< List< Customer > > getCustomers( ) {
        addCustomers( );
        return Optional.of( StreamSupport
                .stream( this.customerRepository.findAll( ).spliterator( ), false )
                .collect( Collectors.toList( ) ) )
                .map( ResponseEntity::ok )
                .orElse( ResponseEntity.ok( Collections.emptyList( ) ) );
    }

    // add customers for demonstration
    public void addCustomers( ) {

        Customer customer1 = new Customer( );
        customer1.setAddress( "1111 foo blvd" );
        customer1.setName( "Foo Industries" );
        customer1.setServiceRendered( "Important services" );
        customerRepository.save( customer1 );

        Customer customer2 = new Customer( );
        customer2.setAddress( "2222 bar street" );
        customer2.setName( "Bar LLP" );
        customer2.setServiceRendered( "Important services" );
        customerRepository.save( customer2 );

        Customer customer3 = new Customer( );
        customer3.setAddress( "33 main street" );
        customer3.setName( "Big LLC" );
        customer3.setServiceRendered( "Important services" );
        customerRepository.save( customer3 );
    }
}
