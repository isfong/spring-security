package com.isfong.spring.security.keycloak.oauth.client.infrastructure.persistence;

import com.isfong.spring.security.keycloak.oauth.client.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository< Customer, Long > {
}
