package com.isfong.spring.security.keycloak.oauth.client.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping( "/users" )
public class UserResource {

    @GetMapping
    public ResponseEntity< Object > getUserInfo( ) throws JsonProcessingException {
        Object principal = SecurityContextHolder.getContext( ).getAuthentication( ).getPrincipal( );
        return ResponseEntity.ok( new ObjectMapper( ).writeValueAsString( principal ) );
    }
}
