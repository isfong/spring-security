package com.isfong.spring.security.anti.violence.infrastructure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder( ) {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder( );
    }

    @Override
    protected void configure( AuthenticationManagerBuilder auth ) throws Exception {
        auth
                .inMemoryAuthentication( )
                .withUser( "tom" )
                .password( passwordEncoder( ).encode( "123" ) )
                .roles( "ADMIN" );
    }
}
