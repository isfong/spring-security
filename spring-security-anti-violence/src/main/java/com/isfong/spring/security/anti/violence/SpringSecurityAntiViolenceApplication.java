package com.isfong.spring.security.anti.violence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityAntiViolenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityAntiViolenceApplication.class, args);
	}

}
