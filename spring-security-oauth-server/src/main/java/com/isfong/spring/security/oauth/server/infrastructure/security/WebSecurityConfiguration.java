package com.isfong.spring.security.oauth.server.infrastructure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity( prePostEnabled = true )
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder( ) {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder( );
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean( ) throws Exception {
        return super.authenticationManagerBean( );
    }

    @Bean
    public PermissionEvaluator permissionEvaluator( ) {
        return new DefaultPermissionEvaluator( );
    }

}
