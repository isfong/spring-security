package com.isfong.spring.security.oauth.server.infrastructure.service;

import com.isfong.spring.security.oauth.server.infrastructure.security.TypeOfLogin;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final HttpServletRequest request;
    private final WechatService wechatService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
        String password = request.getParameter( "password" );
        TypeOfLogin typeOfLogin = TypeOfLogin.valueOf( request.getParameter( "type_of_login" ) );
        UserDetails userDetails;
        switch ( typeOfLogin ) {
            case TEST:
                userDetails = new User( username, passwordEncoder.encode( password ), Collections.emptyList( ) );
                break;
            case WECHAT:
                userDetails = this.wechatLoginHandler( );
                break;
            default:
                throw new UnsupportedOperationException( String.format( "Type of login %s is unsupported.", typeOfLogin ) );
        }
        return userDetails;
    }

    private UserDetails wechatLoginHandler( ) {
        String code = request.getParameter( "password" );
        this.wechatService.login( code );
        return null;
    }
}
