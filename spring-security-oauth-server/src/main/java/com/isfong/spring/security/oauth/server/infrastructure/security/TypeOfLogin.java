package com.isfong.spring.security.oauth.server.infrastructure.security;

public enum TypeOfLogin {
    TEST,
    PHONE_NUMBER,
    WECHAT
}
