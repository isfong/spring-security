package com.isfong.spring.security.oauth.server.domain.model;

import java.util.List;

public class Wechat {
    private String openid;
    private String nickname;
    private int sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private List< String > privilege;
    private String unionid;
}
