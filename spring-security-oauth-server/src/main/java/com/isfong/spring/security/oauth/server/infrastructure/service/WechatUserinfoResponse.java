package com.isfong.spring.security.oauth.server.infrastructure.service;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode( callSuper = true )
public class WechatUserinfoResponse extends WechatResponse {
    private String openid;
    private String nickname;
    private int sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private List< String > privilege;
    private String unionid;
}
