package com.isfong.spring.security.oauth.server.infrastructure.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@RequiredArgsConstructor
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    @Bean
    public TokenStore tokenStore( ) {
        return new JwtTokenStore( jwtAccessTokenConverter( ) );
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter( ) {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory( new ClassPathResource( "jwt.jks" ), "isfong".toCharArray( ) );
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter( );
        jwtAccessTokenConverter.setKeyPair( keyStoreKeyFactory.getKeyPair( "jwt" ) );
        return jwtAccessTokenConverter;
    }

    @Override
    public void configure( AuthorizationServerSecurityConfigurer oauthServer ) {
        oauthServer
                .passwordEncoder( this.passwordEncoder )
                .allowFormAuthenticationForClients( )
                .tokenKeyAccess( "permitAll()" )
                .checkTokenAccess( "isAuthenticated()" );
    }

    @Override
    public void configure( ClientDetailsServiceConfigurer clients ) throws Exception {
        clients
                .inMemory( )
                .withClient( "app" )
                .secret( passwordEncoder.encode( "app_secret" ) )
                .authorizedGrantTypes( "authorization", "password", "refresh_token", "client_credentials" )
                .scopes( "read", "write" );
    }

    @Override
    public void configure( AuthorizationServerEndpointsConfigurer endpoints ) {
        endpoints
                .userDetailsService( userDetailsService )
                .authenticationManager( authenticationManager )
                .accessTokenConverter( jwtAccessTokenConverter( ) )
                .tokenStore( tokenStore( ) );
    }


}
