package com.isfong.spring.security.oauth.server.resource;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
@RequestMapping( "/oauth2/providers" )
public class OAuth2ProviderResource {

    private final RestTemplate restTemplate = new RestTemplate( );
    private final ObjectMapper json = new ObjectMapper( );

    @GetMapping( "/wechat/access_token/{code}" )
    public ResponseEntity< Object > wechatAccessToken( @PathVariable String code ) throws JsonProcessingException {
        ResponseEntity< Object > forEntity = this.restTemplate.getForEntity( OAuth2Provider.WECHAT.buildAccessTokenUri( code ), Object.class );
        System.err.println( json.writeValueAsString( forEntity.getBody( ) ) );
        WechatUserinfo wechatUserinfo = new WechatUserinfo( );
        return ResponseEntity.ok( wechatUserinfo );
    }

    @GetMapping( "/wechat/refresh_token/{refreshToken}" )
    public ResponseEntity< Object > wechatRefreshToken( @PathVariable String refreshToken ) {
        return this.restTemplate.getForEntity( OAuth2Provider.WECHAT.buildRefreshTokenUri( refreshToken ), Object.class );
    }

    @GetMapping( "/wechat/userinfo" )
    public ResponseEntity< Object > wechatRefreshToken( @RequestParam String accessToken, @RequestParam String openid ) {
        return this.restTemplate.getForEntity( OAuth2Provider.WECHAT.buildUserinfoUri( accessToken, openid ), Object.class );
    }
}
