package com.isfong.spring.security.oauth.server.infrastructure.service;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode( callSuper = true )
public class WechatAccessTokenResponse extends WechatResponse {
    private String access_token;
    private long expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
}
