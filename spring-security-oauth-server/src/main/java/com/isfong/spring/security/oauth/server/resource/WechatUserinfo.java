package com.isfong.spring.security.oauth.server.resource;

import lombok.Data;

import java.util.List;

import static java.util.Arrays.asList;

@Data
public class WechatUserinfo {
    private String openid = "wx_open_id";
    private String nickname = "tom";
    private int sex = 1;
    private String province = "广东省";
    private String city = "广州市";
    private String country = "中国";
    private String headimgurl = "https://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0";
    private List< String > privilege = asList( "PRIVILEGE1", "PRIVILEGE2" );
    private String unionid = "wx_union_id";
}
