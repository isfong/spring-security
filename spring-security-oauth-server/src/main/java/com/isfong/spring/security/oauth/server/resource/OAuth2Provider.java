package com.isfong.spring.security.oauth.server.resource;

import org.springframework.lang.Nullable;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public enum OAuth2Provider {
    WECHAT {
        final String accessTokenUri = "https://api.weixin.qq.com/sns/oauth2/access_token";
        final String refreshTokenUri = "https://api.weixin.qq.com/sns/oauth2/refresh_token";
        final String userinfoUri = "https://api.weixin.qq.com/sns/userinfo";
        final String appidKey = "appid";
        final String appid = "wx9bbe0a0a6730df7b";
        final String secretKey = "secret";
        final String secret = "072f28f6d1b43c99c0f556a369928b09";
        final String grantTypeKey = "grant_type";
        final String grantTypeAuthorizationCode = "authorization_code";
        final String grantTypeRefreshToken = "refresh_token";
        final String codeKey = "code";
        final String refreshTokenKey = "refresh_token";
        final String accessTokenKey = "access_token";
        final String openidKey = "openid";

        public URI buildAccessTokenUri( String authorizationCode ) {
            String builder = accessTokenUri +
                    "?" +
                    appidKey + "=" + appid +
                    "&" +
                    secretKey + "=" + secret +
                    "&" +
                    grantTypeKey + "=" + grantTypeAuthorizationCode +
                    "&" +
                    codeKey + "=" + authorizationCode;
            return UriComponentsBuilder.fromUriString( builder )
                    .build( )
                    .toUri( );
        }

        public URI buildRefreshTokenUri( String refreshToken ) {
            String builder = refreshTokenUri +
                    "?" +
                    appidKey + "=" + appid +
                    "&" +
                    grantTypeKey + "=" + grantTypeRefreshToken +
                    "&" +
                    refreshTokenKey + "=" + refreshToken;
            return UriComponentsBuilder.fromUriString( builder )
                    .build( )
                    .toUri( );
        }

        public URI buildUserinfoUri( @Nullable String accessToken, String openid ) {
            String builder = userinfoUri +
                    "?" +
                    accessTokenKey + "=" + accessToken +
                    "&" +
                    openidKey + "=" + openid +
                    "&lang=zh_CN";
            return UriComponentsBuilder.fromUriString( builder )
                    .build( )
                    .toUri( );
        }
    };

    public URI buildAccessTokenUri( String authorizationCode ) {
        return WECHAT.buildAccessTokenUri( authorizationCode );
    }

    public URI buildRefreshTokenUri( String refreshToken ) {
        return WECHAT.buildRefreshTokenUri( refreshToken );
    }

    public URI buildUserinfoUri( String accessToken, String openid ) {
        return WECHAT.buildUserinfoUri( accessToken, openid );
    }
}
