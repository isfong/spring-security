package com.isfong.spring.security.oauth.server.infrastructure.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure( HttpSecurity http ) throws Exception {
        http
                .csrf( ).disable( )
                .sessionManagement( ).sessionCreationPolicy( SessionCreationPolicy.IF_REQUIRED )
                .and( )
                .authorizeRequests( ).antMatchers( "/oauth2/providers/**" ).permitAll( )
                .and( )
                .authorizeRequests( ).anyRequest( ).authenticated( );
    }
}
