package com.isfong.spring.security.oauth.server.infrastructure.service;

import com.isfong.spring.security.oauth.server.resource.OAuth2Provider;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class WechatService {
    private final RestTemplate restTemplate;

    public void login( String code ) {
        WechatAccessTokenResponse wechatAccessTokenResponse = this.restTemplate.getForObject( OAuth2Provider.WECHAT.buildAccessTokenUri( code ), WechatAccessTokenResponse.class );
        this.errorHandler( wechatAccessTokenResponse );
        WechatUserinfoResponse wechatUserinfoResponse = this.restTemplate.getForObject( OAuth2Provider.WECHAT.buildUserinfoUri( wechatAccessTokenResponse.getAccess_token( ), wechatAccessTokenResponse.getOpenid( ) ), WechatUserinfoResponse.class );
        this.errorHandler( wechatUserinfoResponse );
        String openid = wechatUserinfoResponse.getOpenid( );
        String unionid = wechatUserinfoResponse.getUnionid( );
    }

    private void errorHandler( @Nullable WechatResponse response ) {
        assert response != null;
        if ( response.getErrcode( ) != null ) {
            throw new RuntimeException( response.getErrmsg( ) );
        }
    }
}
