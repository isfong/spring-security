package com.isfong.spring.security.oauth.server.infrastructure.service;

import lombok.Data;

@Data
public class WechatResponse {
    private String errcode;
    private String errmsg;
}
